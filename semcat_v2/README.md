Tests texts you can find here:
``aws s3 ls s3://anyclip-weavo-dev/TAF/ml/nlp/text/analyze/``

All that text created from:
``
TEST_URLS = [
    'https://www.eurosport.co.uk/alpine-skiing/pyeongchang/2018/aksel-lund-svindal-wins-gold-for-norway-in-men-s-downhill_sto6620170/story.shtml',
    'https://www.eonline.com/news/914830/fergie-addresses-national-anthem-backlash-i-honestly-tried-my-best/123',
    'https://chicago.suntimes.com/entertainment/ron-cephas-jones-this-is-us-holiday-calendar-netflix-steppenwolf/',
    'https://www.washingtonpost.com/lifestyle/kidspost/storm-reid-felt-an-instant-connection-to-wrinkle-in-time-character/2018/03/05/b5778b4a-1e3b-11e8-9de1-147dd2df3829_story.html?noredirect=on&utm_term=.2dc1fbfa86a9',
    'https://touchdownwire.usatoday.com/2018/10/29/the-chicago-bears-confusing-rise-to-the-top-of-the-nfc-north/',
    'https://venturebeat.com/2018/11/26/rockstar-games-abandons-agent-9-years-after-reveal/',
    'https://venturebeat.com/2018/12/07/airbnb-says-it-has-paid-1-billion-in-local-taxes-on-behalf-of-hosts-since-2014/',
    # Test files
    'https://anyclip-edison.s3.amazonaws.com/SemCat_Tests/TestMovieTVtitles.txt',
    'https://anyclip-edison.s3.amazonaws.com/SemCat_Tests/TestSportsTeams.txt',
    'https://anyclip-edison.s3.amazonaws.com/SemCat_Tests/TestPeopleNames.txt'
]
``
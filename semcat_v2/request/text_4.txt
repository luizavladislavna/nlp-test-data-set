The Chicago Bears' confusing rise to the top of the NFC North. 
The Chicago Bears' confusing rise to the top of the NFC North. 
There’s something different in the air in Chicago this NFL season.

While the Windy City has seen it’s fair share of Bears teams come and go over the years, this year’s squad feels different than ones from years past.

With the Bears teams of the Jay Cutler years, you always expected the squad to put up a good fight on defense, and if the offense could somehow muster the ability to put points on the board, well, that was an added bonus.

But with the rise of Mitch Trubisky and the new offensive scheme of rookie head coach Matt Nagy, this Bears team looks like it can compete with anyone on both sides of the ball.

The Bears started out hot, going 3-1 before dropping their next two to go to 3-3 entering Sunday’s Week 8 showdown with the New York Jets. After a definitive win, one that saw running back Tarik Cohen take a short screen pass 70 yards for a touchdown, the Bears moved their record to 4-3, and are now sitting pretty atop the NFC North standings, thanks to losses by the the Packers, Vikings and Lions.

But, it’s been a strange two weeks for Chicago when it comes to their division ranking.

The NFL is a wild and crazy league. And that’s why, they play the game.

The Bears will look to continue their winning ways when they travel to Buffalo in Week 9 to take on the struggling Bills.
# HOST='pcn-dev-nlp-in.anyclipsrv.info'
HOST='localhost'
VERSION='V2'

echo $HOST

for i in $(ls -d */);
    do
        cat ${i}request.json | curl -X POST --header 'Content-Type: application/json'  --data-binary "@-"  'http://'$HOST':7000/v1/api/text/analyze/' -o ${i}response_$VERSION.json;
done
0. http://www.eonline.com/news/914830/fergie-addresses-national-anthem-backlash-i-honestly-tried-my-best/
- OK

1. https://www.washingtonpost.com/lifestyle/kidspost/storm-reid-felt-an-instant-connection-to-wrinkle-in-time-character/2018/03/05/b5778b4a-1e3b-11e8-9de1-147dd2df3829_story.html?noredirect=on&utm_term=.2dc1fbfa86a9
- OK (without NIL)

2. https://touchdownwire.usatoday.com/2018/10/29/the-chicago-bears-confusing-rise-to-the-top-of-the-nfc-north/
- OK (without NIL)

3. https://chicago.suntimes.com/entertainment/ron-cephas-jones-this-is-us-holiday-calendar-netflix-steppenwolf/
- MISMATCH for last item (that is known bug from you script) - deletion not working

4. https://www.eurosport.co.uk/alpine-skiing/pyeongchang/2018/aksel-lund-svindal-wins-gold-for-norway-in-men-s-downhill_sto6620170/story.shtml
- OK

5. https://www.businessinsider.com/meghan-markle-fashion-influence-compared-to-kate-middleton-2018-1
- OK

6. https://www.joe.co.uk/entertainment/jeremy-kyle-guests-ed-sheeran-lookalike-exes-leave-viewers-in-hysterics-157876
- should be OK (mismatch for "Medical Health" - depends on english-text-analyzer: something strange with text "... her ‘abusive, druggie’ ex ..." When I run that locally, I have 2 words "‘abusive" and "druggie’" and should be without "‘" - it works fine in docker container)

7. https://economictimes.indiatimes.com/magazines/panache/the-elon-musk-biography-should-be-made-into-a-movie/articleshow/62535806.cms
- OK (without NIL)

8. https://www.chicagotribune.com/suburbs/skokie/community/chi-ugc-article-students-present-solar-eclipse-findings-at-na-2018-01-16-story.html
- OK

9. https://www.coindesk.com/as-bitcoins-slide-continues-prices-look-towards-8k
- OK

10. https://www.theinquirer.net/inquirer/news/2475805/iphone-x-apple-might-restart-production-in-certain-markets
- OK


### How to test:
- `chmod +x test.sh`
- `./test.sh`
